class newCard {
    constructor(){
        this.card = document.createElement('div');
        this.card.classList.add('card');
        this.card.style = 'width: 18rem;';
        this.dropZone = '<div class="card-header" id="drop_zone" ondrop="newSpeciesCard.dropHandler(event);" ondragover="newSpeciesCard.dragOverHandler(event);">\n' +
            '  <p>Drag one or more files to this Drop Zone ...</p>\n' +
            '</div>';
        this.cardbody = document.createElement('div');
        this.cardbody.classList.add('card-body');
        this.title = document.createElement("h5");
        this.title.classList.add('card-title');
        this.text = document.createElement('p');
        this.text.classList.add('card-text');

        this.btnClose = document.createElement('btn');
        this.btnClose.setAttribute('type', 'button')
        this.btnClose.classList.add('close');
        this.btnClose.setAttribute('aria-label', 'Close');

        this.spanClose = document.createElement('span');
        this.spanClose.setAttribute('aria-hidden', 'true');
        this.spanClose.innerHTML = '&times;';
        this.spanClose.id = `btnClose-${this.id}`;

        this.btnSave = document.createElement('btn');
        this.btnSave.setAttribute('type', 'button')
        this.btnSave.classList.add('btn-success');
        this.btnSave.setAttribute('aria-label', 'Save');
        this.spanSave = document.createElement('span');
        this.spanSave.setAttribute('aria-hidden', 'true');
        this.spanSave.innerHTML = '&#x1F5AB;';
        this.spanSave.id = `btnSave`;
        this.id = null;
        this.form = `
            <form id="nySopp">
                <div class='form-group'>
                    <label for="id">ID:</label>
                    <input type="text" disabled class="form-control" id="id">
                    <label for="norsk">Norsk navn: </label>
                    <input aria-describedby="norskTekst" type='text' class="form-control" placeholder="offisielt norsk navn" name="norsk" id="norsk">
                    <small id="norskTekst class="form-text text-muted">Stav navnet slik det står i normlisten, stor forbokstav</small><br>
                    <label for="english">Engelsk navn: </label>
                    <input type='text' class="form-control" placeholder="valgfritt" name="english" id="english">
                    <label for="genus">Slektsnavn latin: </label>
                    <select name="genus" id="genus" class="form-control"><option value="null" selected></option> </select>
                    <label for="latin">Artsnavn latin</label>
                    <input type="text" name="latin" id="latin" class="form-control">
                </div>
                <div class="form-group">
                    <label for "spiselighet">Spiselighet</label>
                    <select aria-describedby="Spiselighet" class="form-control" name="spiselighet" id="spiselighet" onchange="newSpeciesCard.giftOnOff()">
                        <option value="null">-</option>
                    </select><br>
                    <label for="hatt">Hatt: </label>
                    <input type="text" name="hatt" class="form-control" aria-describedby="Hatt" id="hatt">
                    <label for="type">Sopptype: </label>
                    <select name="type" id="type"><option value="null">-</option> </select><br>
                    <label for="underside">Underside: </label>
                    <input type="text" name="underside" class="form-control" id="underside">
                    <label for="stil">Stilk: </label>
                    <input type="text" name="stilk" class="form-control" id="stilk">
                    <label for="sporepulver">Sporepulver: </label>
                    <input type="text" class="form-control" name="sporepulver" id="sporepulver">
                    <label for="voksested">Voksested: </label>
                    <input type="text" name="voksested" class="form-control" id="voksested">
                    <label for="merknad">Merknad: </label>
                    <input type="text" name="merknad" class="form-control" id="merknad">
                    <label for="giftsymptomer" style="display: none" id="labelGiftsymptomer">Giftsymptomer: </label>
                    <input type="text" name="giftsymptomer" class="form-control" id="giftsymptomer" style="display: none">
                    <label for="forvekslingsarter">Forvekslingsarter: <div id="divForvekslingsarter"></div></label>
                    <select class="form-control" name="forvekslingsarter" id="forvekslingsarter" onchange="newSpeciesCard.addForveksling()">
                        <option value="null">ingen</option>
                    </select>
                    <div id="imagefiles"></div>
                    <label for="imageFile">Bilde filnavn</label>
                    <input type="text" class="form-control" id="imageFile" name="imagefile">
                    <button type="submit" id="submitImageFilename">Legg til bildefil</button>
                </div>
                    
            </form>`;

        this.data = {};
        this.data.forvekslingsarter = [];
        this.speciesList = {}
        this.compileCard();
        this.fileNames = [];
    }

    compileCard(){
        this.btnClose.appendChild(this.spanClose);
        this.btnSave.appendChild(this.spanSave);
        this.cardbody.appendChild(this.btnSave);
        this.cardbody.appendChild(this.btnClose);
        this.card.appendChild(this.btnSave);
        this.card.appendChild(this.btnClose);

        // this.card.innerHTML = this.dropZone + this.card.innerHTML + this.form;
        this.card.innerHTML = this.card.innerHTML + this.form;



    }

    imageUpload(){

    }

    dropHandler(ev) {
        console.log('File(s) dropped');

        // Prevent default behavior (Prevent file from being opened)
        ev.preventDefault();

        if (ev.dataTransfer.items) {
            // Use DataTransferItemList interface to access the file(s)
            for (let i = 0; i < ev.dataTransfer.items.length; i++) {
                // If dropped items aren't files, reject them
                if (ev.dataTransfer.items[i].kind === 'file') {
                    let file = ev.dataTransfer.items[i].getAsFile();
                    console.log('... file[' + i + '].name = ' + file.name);
                }
            }
        } else {
            // Use DataTransfer interface to access the file(s)
            for (let i = 0; i < ev.dataTransfer.files.length; i++) {
                console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
            }
        }
    }

    dragOverHandler(ev) {
        console.log('File(s) in drop zone');

        // Prevent default behavior (Prevent file from being opened)
        ev.preventDefault();
    }

    saveCard(){
        if(this.id == null) {
            this.data.norsk = document.getElementById('norsk').value;
            this.data.genus = document.getElementById('genus').value;
            this.data.latin = document.getElementById('latin').value;
            this.data.english = document.getElementById('english').value;
            this.data.spiselighet = document.getElementById('spiselighet').value;
            this.data.hatt = document.getElementById('hatt').value;
            this.data.type = document.getElementById('type').value;
            this.data.underside = document.getElementById('underside').value;
            this.data.stilk = document.getElementById('stilk').value;
            this.data.sporepulver = document.getElementById('sporepulver').value;
            this.data.voksested = document.getElementById('voksested').value;
            this.data.merknad = document.getElementById('merknad').value;
            this.data.giftsymptomer = document.getElementById('giftsymptomer').value;


            fetch('ajax.php?newcard=true&saveCard=true', {
                method: 'POST', // or 'PUT'
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(this.data),
            })
                .then(response => response.json())
                .then(data => {
                    console.log('Success:', data);
                    document.getElementById('id').value = data;
                    this.data.id = data;
                    this.id = data;
                    if (this.fileNames.length > 0) {
                        this.uploadImageFilenames();
                    }
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        }
    }

    uploadImageFilenames (){
        let length = this.fileNames.length;
        for(let i = 0; i<length ; i++){
            fetch(`ajax.php?newcard=true&imageFileName=${this.fileNames[i]}&id=${this.data.id}`)
            this.fileNames.splice(i, 1);
        }
    }
    getSpiselighet(){

        fetch('ajax.php?newcard=true&spiselighet=true')
            .then(response => response.json())
            //.then(data => console.log(data))
            .then(data => this.setSpiselighet(data));
    }
    setSpiselighet(data) {
        let element = document.getElementById('spiselighet');
        element.innerHTML = '';

        for (const obj in data) {

            let option = document.createElement("option");
            option.text = data[obj].description;
            option.value = data[obj].id;
            element.add(option);
        }
    }

    getSopptype(){

        fetch('ajax.php?newcard=true&sopptype=true')
            .then(response => response.json())
            //.then(data => console.log(data))
            .then(data => this.setSopptype(data));
    }
    setSopptype(data) {
        let element = document.getElementById('type');
        element.innerHTML = '';
        for (const obj in data) {

            let option = document.createElement("option");
            option.text = data[obj].type;
            option.value = data[obj].id;
            element.add(option);
        }
    }

    getSoppliste(){

        fetch('ajax.php?newcard=true&getall=true')
            .then(response => response.json())
            //.then(data => console.log(data))
            .then(data => this.setSoppliste(data));
    }
    setSoppliste(data) {
        let element = document.getElementById('forvekslingsarter');
        element.innerHTML = '';
        let option = document.createElement("option");
        option.text = 'ikke satt';
        option.value = null;
        element.add(option);
        for (const obj in data) {
            option = document.createElement("option");
            option.text = data[obj].norwegian;
            option.value = data[obj].id;
            element.add(option);
        }
    }

    getGenus(){
        fetch('ajax.php?newcard=true&genus=true')
            .then(response => response.json())
            //.then(data => console.log(data))
            .then(data => this.setGenus(data));
    }
    setGenus(data) {
        let element = document.getElementById('genus');
        element.innerHTML = '';
        for (const obj in data) {
            let option = document.createElement("option");
            option.text = data[obj].latin;
            option.value = data[obj].id;
            element.add(option);
        }
    }

    addForveksling(){
        let element = document.getElementById('forvekslingsarter');
        let obj = {id: element.value, norwegian: element[element.selectedIndex].text};
        console.log(obj);
        this.data.forvekslingsarter.push(obj);
        let div = document.getElementById('divForvekslingsarter');
        let newDiv = document.createElement('div');
        newDiv.id = `forveksling-${element.value}`;
        let btnDelete = document.createElement("button");
        let spanDelete = document.createElement("span");
        spanDelete.id = `btnDeleteForveksling-${element.value}`
        spanDelete.setAttribute('aria-hidden', 'true');
        spanDelete.innerHTML = '&times;';
        //spanDelete.addEventListener('click', card.delForveksling(element.value));
        btnDelete.appendChild(spanDelete);
        newDiv.innerHTML = element[element.selectedIndex].text;
        newDiv.appendChild(btnDelete);
        div.appendChild(newDiv);
    }

    delForveksling(id){
        console.log(`Slett forvekslingsart ${id}`);
        for(let i = 0; i < this.data.forvekslingsarter.length; i++){
            if(this.data.forvekslingsarter[i].id == id){
                this.data.forvekslingsarter = this.data.forvekslingsarter.slice(i, 1);
                document.getElementById(`forveksling-${id}`).remove();
            }
        }
    }

    giftOnOff(){

        let giftstatus = document.getElementById('spiselighet');
        let labelGiftsymptomer = document.getElementById('labelGiftsymptomer');
        let giftsymptomer = document.getElementById('giftsymptomer');

        if(giftstatus.value == 1 || giftstatus.value == 2){
            labelGiftsymptomer.style.display = 'inline';
            giftsymptomer.style.display = 'inline';
        }else{
            labelGiftsymptomer.style.display = 'none';
            giftsymptomer.style.display = 'none';
        }
    }


}
let newSpeciesCard = new newCard();
function newSpecies(){
    newSpeciesCard = new newCard();
    document.getElementById('cardgroup').innerHTML = '';
    document.getElementById('cardgroup').appendChild(newSpeciesCard.card);
    newSpeciesCard.getSpiselighet();
    newSpeciesCard.getSopptype();
    newSpeciesCard.getSoppliste();
    newSpeciesCard.getGenus();
}
