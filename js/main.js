function startQuiz(){
    let script = document.createElement('script');
    script.src = 'js/quiz.js';
    script.onload = () => {
        objQuiz.quizSettings();
    }
    document.body.appendChild(script);
}
let userAccessLevel = 0;
let userId = undefined;
function checkUserAccess(){
    fetch('ajax.php?checkUserAccess=true')
        .then(response => response.json())
        .then(data => {
            console.log('User Access level: ' + JSON.stringify(data));
            if(data >= 2){

                document.getElementById('nyttArtskort').style.display = 'inline';
                document.getElementById('bilderBehandle').style.display = 'inline';
            }
            userAccessLevel = data;
        })
    fetch('ajax.php?getCurrentUserId=true')
        .then(response => response.json())
        .then(data =>{
            userId = data;
        })
}
checkUserAccess();
let bildeType;
function bilderOpplasting(){
    fetch('bilder.php')
        .then(response => response.text())
        .then(data => {
            mainDiv = document.getElementById('divMain');
            newDiv = document.createElement('div');
            mainDiv.appendChild(newDiv);
            newDiv.innerHTML = data;
            document.getElementById('divLeft').innerHTML = '';
            let script = document.createElement('script');
            script.src = 'js/bilder.js';
            document.body.appendChild(script);
        })


}