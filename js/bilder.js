class bilder{
    constructor() {
        const ajaxURL = 'ajax.php?image=true';
        this.ajaxURL = ajaxURL;
        this.users = {};
        this.licenses = {};
        this.selectedSpecies = undefined;
        // fetch(ajaxURL + '&getAllSpecies=true')
        //     .then(response => response.json())
        //     .then(data =>{
        //         this.species = data;
        //         this.listAllSpecies();
        //     })
        this.listAllSpecies();

        // fetch(ajaxURL + '&getAllImages=true')
        //     .then(response => response.json())
        //     .then(json => {
        //         this.images = json;
        //     })

        this.getAllImages();
        fetch(ajaxURL + '&getAllUsers=true')
            .then(response => response.json())
            .then(json => {
                for(let user of json){
                    console.log(user);
                    const userid = user.id.toString();
                    this.users[userid] = user;
                }
            })
        fetch(ajaxURL + '&getAllLicenses=true')
            .then(response => response.json())
            .then(json => {
                for(let license of json){
                    const id = license.id.toString();
                    this.licenses[id] = license;
                    const option = document.createElement('option')
                    option.value = license.id;
                    option.text = license.licenseName;
                    document.querySelector('#uploadImgLicense').add(option);
                }
            })
        const divs = document.querySelectorAll('.bildebehandling');
        for (let i = 0; i < divs.length ; i++){
            divs[i].classList.remove('hidden');
        }
        const imageUpload = document.querySelector('input');
        imageUpload.addEventListener('change', (el) =>{
            this.previewUpload();
        })
    }

    getAllImages(){
        fetch(this.ajaxURL + '&getAllImages=true')
            .then(response => response.json())
            .then(json => {
                this.images = json;
            })
    }
    previewUpload(){
        const file = document.querySelector('input[type=file]').files[0]
        const preview = document.querySelector('#uploadedImg')
        const reader = new FileReader();


        reader.onloadend = function (){
            preview.src = reader.result;
        }

        if (file) {
            reader.readAsDataURL(file);
            document.querySelector('#upladedImgProperties').classList.remove('hidden');
            this.buttonListeners();
        }else{
            preview.src = "";
            document.querySelector('#upladedImgProperties').classList.add('hidden');
        }
    }

    buttonListeners(){

        const saveButton = document.querySelector('#saveImage');
        saveButton.addEventListener('click', ()=>{
            const file = document.querySelector('input[type=file]').files[0];
            const license = document.querySelector('#uploadImgLicense').value;
            const formData = new FormData();
            const preview = document.querySelector('#uploadedImg')
            formData.append('newImage', file);
            formData.append('species', this.selectedSpecies);
            formData.append('license', license);
            fetch('ajax.php?fileUpload=true',{
                method: 'POST',
                body: formData,
            })
                .then(response => response.json())
                .then(result =>{
                    console.log('Success:', result);
                    //this.openSpecies(document.querySelector('#artsliste'));
                    document.querySelector('input[type=file]').value = '';
                    preview.src = '';
                    const el = document.querySelector('#artsliste');
                    const ajaxURL = 'ajax.php?image=true';
                    fetch(ajaxURL + '&getAllImages=true')
                        .then(response => response.json())
                        .then(json => {
                            this.images = json;
                        })
                        .then(this.openSpecies(el));
                })
                .catch(error => {
                    console.error('Error:', error);
                })
        })
    }
    arrayToObject(array){
        array.reduce((obj, item) => {
            obj[item.id] = item
            return obj
        }, {})}
    listForApproval(){

    }
    listAllSpecies(){
        const ajaxURL = 'ajax.php?image=true';
        const select = document.getElementById('artsliste');
        select.innerHTML = '';
        const initOption = document.createElement("option");
        initOption.text = '-';
        initOption.selected = true;
        select.appendChild(initOption);
        fetch(ajaxURL + '&getAllSpecies=true')
            .then(response => response.json())
            .then(data =>{
                this.species = data;
                for (const element of this.species){
                    const option = document.createElement('option');
                    option.value = element.id;
                    option.text = `${element.norwegian} - (${element.genus} ${element.latin})`
                    select.add(option);
                }
            })

        select.addEventListener("change", (el) => {

            this.selectedSpecies = el.target.value;
            this.openSpecies(el)
            document.querySelector('#uploadDiv').classList.remove('hidden');
        });
    }

    openSpecies(el){

        const cardgroup = document.getElementById('bilderPerArt');
        cardgroup.innerHTML = '';
        for (let image of this.images) {
            //console.log(image.species);
            //if(image.species == el.target.value) {
            if(image.species == this.selectedSpecies) {
                console.log(image.species);
                const card = document.createElement('div');
                card.classList.add('card');
                card.style = 'width: 18rem;';
                card.id = `image-${image.id}`;
                const cardimage = document.createElement('img');
                cardimage.classList.add('card-img-top');
                cardimage.src = 'images/' + image.filename;
                const btnApprove = document.createElement('button');
                btnApprove.innerText = 'Godkjenn';
                btnApprove.addEventListener('click', () =>{
                    this.approveImage(image.id);
                    card.classList.remove('image-pending', 'image-rejected');
                    card.classList.add('image-approved');
                })

                const btnReject = document.createElement('button');
                btnReject.innerText = 'Avvis';
                btnReject.addEventListener('click', () =>{
                    this.rejectImage(image.id);
                    card.classList.remove('image-approved', 'image-pending');
                    card.classList.add('image-rejected');
                })

                const btnDelete = document.createElement('button');
                btnDelete.innerText = 'Slett';
                btnDelete.addEventListener('click', () =>{
                    this.deleteImage(image.id);
                    card.remove();
                })

                const cardbody = document.createElement('div');
                cardbody.classList.add('card-body');
                if (image.approved === 0){
                    card.classList.add('image-pending');
                    cardbody.appendChild(btnApprove);
                    cardbody.appendChild(btnReject)
                } else if (image.approved === 3){
                    card.classList.add('image-rejected');
                    cardbody.appendChild(btnApprove);
                } else if (image.approved === 1){
                    card.classList.add('image-approved');
                }
                if(image.copyright === userId){
                    cardbody.appendChild(btnDelete);
                }
                // btnDelete.classList.add('btn', 'btn-primary');
                // btnReject.classList.add('btn', 'btn-primary');
                // btnApprove.classList.add('btn', 'btn-primary');
                const copyright = document.createElement('div');
                copyright.innerHTML = `${this.users[image.copyright].name} <${this.users[image.copyright].email}>`;
                const lisens = document.createElement('a');
                lisens.href = this.licenses[image.license].url;
                lisens.innerText = this.licenses[image.license].licenseName;
                lisens.title = this.licenses[image.license].licenseText;


                // compile card
                cardbody.appendChild(copyright);
                cardbody.appendChild(lisens);
                card.appendChild(cardimage);
                card.appendChild(cardbody);
                cardgroup.appendChild(card);


            }
        }
    }

    rejectImage(id){
        const ajaxURL = 'ajax.php?image=true&';
        fetch(ajaxURL + `rejectImage=${id}`)
            .then(result => result.json())
            .then(result => console.log(result))
        this.getAllImages();
    }
    approveImage(id){
        const ajaxURL = 'ajax.php?image=true&';
        fetch(ajaxURL + `approveImage=${id}`)
            .then(result => result.json())
            .then(result => console.log(result))
        this.getAllImages();
    }
    deleteImage(id){
        const ajaxURL = 'ajax.php?image=true&';
        fetch(ajaxURL + `deleteImage=${id}`)
            .then(result => result.json())
            .then(result => console.log(result))
        this.getAllImages();
    }




}
const objBilder = new bilder;
