class quiz{
    constructor() {
        this.settingsForm = `
            <form id='quizSettings'>
                <div class="form-check">
                    <input class="form-check-input" name="quizType" id="quizTypeBilde" value="bilde" type="radio" checked>
                    <label class="form-check-label" for="quizTypeBilde">Bildequiz</label>
                </div>
                
                <div class="form-check">
                    <input class="form-check-input" name="quizType" id="quizTypeTekst" value="tekst" type="radio">
                    <label class="form-check-label" for="quizTypeTekst">Tekstquiz</label>
                </div>
                
                <div class="form-check">
                    <input class="form-check-input" name="quizAlternativ" id="quizAlternativJa" value="true" type="radio" checked>
                    <label class="form-check-label" for="quizTypeBilde">Med alternativ</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" name="quizAlternativ" id="quizAlternativNei" value="false" type="radio">
                    <label class="form-check-label" for="quizTypeBilde">Uten alternativ</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" name="quizKunnskap" id="quizKunnskapJa" value="true" type="radio" checked>
                    <label class="form-check-label" for="quizTypeBilde">Bare arter jeg ikke kan godt</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" name="quizKunnskap" id="quizKunnskapNei" value="false" type="radio">
                    <label class="form-check-label" for="quizTypeBilde">Alle arter</label>
                </div>
                <div class="form-group">
                    <label for="quizAntall">Antall spørsmål</label>
                    <select class="form-control" id="quizAntall" name="quizAntall" data-width="auto"></select>
                </div>
                <button type="submit" class="btn btn-primary" onclick="objQuiz.startQuiz()">Start quiz</button>
            </form>
        `
        let div = document.getElementById('divLeft');
        div.innerHTML = this.settingsForm;
        this.form = document.getElementById('quizSettings');
        this.form.onsubmit = function(event) {
            event.preventDefault();
            return false;
        }
        this.speciesCount = 0;
        this.cardSettings = {quizType: undefined, antall: undefined, alternativ: undefined, kunnskap: undefined};
        this.answers = {};
        this.correctCount = 0;
        this.wrongCount = 0;
        this.answeredWrong = [];
        this.questionCounter = 0;
    }

    quizSettings(){

        fetch('ajax.php?quiz=true&getCount=true')
            .then(result => result.json())
            .then(data => this.speciesCount = data)
            .then(() => {
                let select = document.getElementById('quizAntall');
                console.log(this.speciesCount);
                for(let i = 0;i < this.speciesCount ; i++){
                    let option = document.createElement("option");
                    option.text = i+1;
                    option.value = i+1;
                    select.add(option);
                }
            })
    }
    quizcard(){
        this.card = document.createElement('div');
        this.card.classList.add('card');
        this.card.style = 'width: 18rem;';
        this.image = document.createElement('img');
        this.image.classList.add('card-img-top');
        this.image.id = 'quizImage';
        this.cardbody = document.createElement('div');
        this.cardbody.classList.add('card-body');
        this.quizForm = document.createElement("form");
        this.quizForm.id = 'quizForm';
    }

    startQuiz(){
        this.cardSettings.quizType = this.form.elements['quizType'].value;
        this.cardSettings.alternativ = JSON.parse(this.form.elements['quizAlternativ'].value);
        this.cardSettings.kunnskap = this.form.elements['quizKunnskap'].value;
        this.cardSettings.antall = document.getElementById('quizAntall').value;
        console.log('Start quiz!');
        fetch(`ajax.php?quiz=true&antall=${this.cardSettings.antall}&type=${this.cardSettings.quizType}&kunnskap=${this.cardSettings.kunnskap}`)
            .then(response => response.json())
            .then(data => this.questions = data)
            .then(data => {
                console.log('data' + JSON.stringify(data));
                document.getElementById('divLeft').innerHTML = '';

                this.bildeQuiz(this.cardSettings.alternativ);

        });
    }

    bildeQuiz(alternatives = true){
        this.quizcard();
        this.card.appendChild(this.image)


        document.getElementById('cardgroup').innerHTML = '';
        console.log("startet bildequiz");
        let maindiv = document.getElementById('cardgroup');

        this.cardbody.appendChild(this.quizForm);
        this.card.appendChild(this.cardbody);
        maindiv.appendChild(this.card);
        let bilde = document.getElementById('quizImage');
        fetch('ajax.php?quiz=true&getBilder=' + this.questions[this.questionCounter].id)
            .then(result => result.json())
            .then(data => {
                bilde.src = 'images/' + data[0].filename;

            })
        let form = document.getElementById('quizForm');
        form.onsubmit = (e) =>{
            e.preventDefault();
        }
        if(alternatives){
            for(let i = 0; i < 4;i++) {
                let altDiv = document.createElement('div');
                altDiv.classList.add('form-check');
                let alternativ = document.createElement("input")
                alternativ.type = 'radio';
                alternativ.value = i;
                alternativ.classList.add('form-check-input');
                alternativ.name = 'svar';
                alternativ.id = `svaralternativ-${i}`;
                altDiv.appendChild(alternativ);
                let altLabel = document.createElement("label");
                altLabel.classList.add('form-check-label');
                altLabel.htmlFor = `svaralternativ-${i}`;
                altLabel.id = `svaralternativLabel-${i}`;
                altDiv.appendChild(altLabel);
                form.appendChild(altDiv);
            }
        }else{
            let altDiv = document.createElement('div');
            altDiv.classList.add('form-check');
            let alternativ = document.createElement("select")
            alternativ.id = 'quizAnswer';
            let option = document.createElement('option');
            option.value = null;
            option.text = '';
            alternativ.add(option);
            fetch('ajax.php?cards=true&listAllCards=true&sortByNorwegian=true')
                .then(response => response.json())
                .then(data => {
                    for(let i=0;i<data.length;i++){
                        option = document.createElement('option');
                        option.value = data[i].id;
                        option.text = `${data[i].norwegian} (${data[i].genus} ${data[i].latin})`;
                        alternativ.add(option);
                    }
                })
                .then(altDiv.appendChild(alternativ))
                .then(altDiv.appendChild(document.createElement('br')))
                .then(form.appendChild(altDiv));
        }


        let button = document.createElement("button");
        button.classList.add('btn', 'btn-primary');
        button.id = 'quizSubmitBtn';
        button.textContent = 'Svar';
        form.appendChild(button);
        console.log('Questions: ' + this.questions);
        //for(let i = 0; i < this.questions.length; i++){
        let i = this.questionCounter;
        if(alternatives){
            let alternatives = [];

            fetch(`ajax.php?quiz=true&alternatives=3&not=${this.questions[i].id}`)
                .then(response => response.json())
                .then(data => {
                    alternatives.push(...data);
                    console.log(alternatives);
                    alternatives.push(this.questions[i]);
                    this.shuffleArray(alternatives);
                    for (let j = 0; j<alternatives.length;j++){
                        let label = document.getElementById(`svaralternativLabel-${j}`);
                        label.innerHTML = `${alternatives[j].norwegian} - (${alternatives[j].genus} ${alternatives[j].latin})`;
                        // if(alternatives[j].id === this.questions[i]){
                        //     this.answers[i] = j;
                        // }
                        let radio = document.getElementById(`svaralternativ-${j}`)
                        radio.value = alternatives[j].id
                    }
                    let btn = document.getElementById('quizSubmitBtn');
                    this.questionCounter ++;
                    btn.addEventListener('click', () => {
                        let selected = form.elements['svar'].value;
                        if(selected == this.questions[i].id){
                            this.correctCount ++;
                            this.editProgress(this.questions[i].id, 1);

                        }else{
                            this.wrongCount ++;
                            this.editProgress(this.questions[i].id, -2);
                            this.answeredWrong.push(this.questions[i]);
                        }

                        if(this.questionCounter == this.questions.length){
                            this.endQuiz();
                        }else{

                            this.bildeQuiz();
                        }
                    });
                });
        }else{
            let btn = document.getElementById('quizSubmitBtn');
            this.questionCounter ++;
            btn.addEventListener('click', () => {
                let selected = document.getElementById('quizAnswer').value;
                if (selected == this.questions[i].id) {
                    this.correctCount++;
                    this.editProgress(this.questions[i].id, 2);

                } else {
                    this.wrongCount++;
                    this.editProgress(this.questions[i].id, -1);
                    this.answeredWrong.push(this.questions[i]);
                }

                if (this.questionCounter == this.questions.length) {
                    this.endQuiz();
                } else {
                    this.bildeQuiz(false);
                }
            });
        }
        }
        endQuiz(){
            document.getElementById('cardgroup').innerHTML = '';
            let html = `<p>
            Antall riktige svar: ${this.correctCount} <br>
            Antall feil svar: ${this.wrongCount} <br>
            du må øve mer på:
        `;
            for(let i = 0; i < this.answeredWrong.length ; i++) {
                html = `${html} ${this.answeredWrong[i].norwegian}, `
            }
            html = html + '</p>';
            html = html + `<a href="#" onclick="resetQuiz()">Ny quiz?</a>`;
            document.getElementById('divLeft').innerHTML = html;
    }
    editProgress(id, value){
        console.log(`Id: ${id} endring: ${value}`);
        fetch(`ajax.php?quiz=true&updateProgress=${id}&value=${value}`)
    }
    shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }
}

let objQuiz = new quiz();
function resetQuiz(){
    document.getElementById('cardgroup').innerHTML = '';
    objQuiz = new quiz();
    startQuiz();
}