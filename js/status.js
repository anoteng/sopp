function visStatus(){
    container = document.getElementById('divMain');
    document.getElementById('cardgroup').innerHTML='';
    fetch('ajax.php?quiz=true&getStatus=true')
        .then(response => response.json())
        .then(data => {
            for(let i = 0;i<data.length;i++){
                let progress = data[i].level/5*100;
                if (progress > 100){
                    progress = 100;
                } else if (progress < 0){
                    progress = 0;
                }
                let progressDiv = document.createElement('div');
                progressDiv.classList.add('progress');
                let bar = document.createElement('div');
                bar.classList.add('progress-bar', 'progress-bar-striped', 'progress-bar-animated');
                bar.style.width = `${progress}%`;
                bar.setAttribute('role', 'progressbar');
                bar.setAttribute('aria-valuenow', progress);
                bar.setAttribute('aria-valuemin', 0);
                bar.setAttribute('aria-valuemax', 100);
                if(progress >= 100){
                    bar.classList.add('bg-success');
                }else if(progress >= 75){
                    bar.classList.add('bg-info');
                } else {
                    bar.classList.add('bg-danger');
                }
                bar.innerHTML = `${data[i].norwegian} ${data[i].level}/5`;
                progressDiv.appendChild(bar);
                container.appendChild(progressDiv);
            }
        })
}