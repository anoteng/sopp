class Cards {
    constructor(id){
        fetch(`ajax.php?cards=true&singlecard=${id}`)
            .then(response => response.json())
            .then(data => this.data = data)
            .then(() => this.getImages())
            .then(() => this.getForveksling())
            .then(() => console.log(this.data));

        this.card = document.createElement('div');
        this.card.classList.add('card');
        this.card.style = 'width: 18rem;';
        this.image = document.createElement('img');
        this.image.classList.add('card-img-top');
        this.image.id = `image-${id}`
        this.cardbody = document.createElement('div');
        this.cardbody.classList.add('card-body');
        this.title = document.createElement("h5");
        this.title.classList.add('card-title');
        this.text = document.createElement('p');
        this.text.classList.add('card-text');
        this.btnA = document.createElement('a');
        this.btnA.classList.add('btn', 'btn-primary');
        this.btnA.href = '#';
        this.btnB = document.createElement('a');
        this.btnB.classList.add('btn', 'btn-primary');
        this.btnB.href = '#';
        this.btnC = document.createElement('a');
        this.btnC.classList.add('btn', 'btn-primary');
        this.btnC.href = '#';
        this.btnD = document.createElement('a');
        this.btnD.classList.add('btn', 'btn-primary');
        this.btnD.href = '#';
        this.buttons = 0;
        this.id = id;
        this.btnClose = document.createElement('btn');
        this.btnClose.setAttribute('type', 'button')
        this.btnClose.classList.add('close');
        this.btnClose.setAttribute('aria-label', 'Close');

        this.spanClose = document.createElement('span');
        this.spanClose.setAttribute('aria-hidden', 'true');
        this.spanClose.innerHTML = '&times;';
        this.spanClose.id = `btnClose-${this.id}`;

    }

    compileCard(){
        this.btnClose.appendChild(this.spanClose);
        this.cardbody.appendChild(this.btnClose);
        this.cardbody.appendChild(this.title);
        this.cardbody.appendChild(this.title);
        this.cardbody.appendChild(this.text);
        this.card.appendChild(this.image);
        this.card.appendChild(this.cardbody);
        this.card.id = `card-${this.id}`;

    }

    getForveksling() {
        fetch(`ajax.php?cards=true&forvekslingsarter=${this.data.id}`)
            .then(response => response.json())
            .then(data => this.forvekslingsarter = data)
            .then(() => this.fillCard());
    }

    fillCard(){
        this.title.innerHTML = `${this.data.norwegian} - (${this.data.genus} ${this.data.latin})`;
        let edible = '';
        if (this.data.edible === 1 || this.data.edible === 2){
            edible = `<p><b>Giftsymptomer: </b> ${this.data.forgiftningssymptomer}`
        }else{
            edible = '';
        }
        this.text.innerHTML = `
            <p><b>Engelsk navn: </b>${this.data.english}</p>
            <p><b>Spiselighet: </b>${this.data.edibleDesc}</p>
            <p><b>Hatt: </b>${this.data.hatt}</p>
            <p><b>${this.data.typeDesc}: </b>${this.data.underside}</p>
            <p><b>Stilk: </b>${this.data.stilk}</p>
            <p><b>Sporepulver: </b>${this.data.sporepulver}</p>
            <p><b>Voksested: </b>${this.data.voksested}</p>
            ${edible}
            <p><b>Merknad: </b>${this.data.merknad}</p>
            <p><b>Forvekslingsarter: </b>${this.forvekslingsarter}</p>`;
        this.compileCard();
    }

    getImages(){
        fetch(`ajax.php?cards=true&imagelist=${this.id}`)
            .then(response => response.json())
            .then(data => this.filelist = data)
            .then(() => this.image.src = `images/${this.filelist[0].filename}`)
            .then(() => this.imageIndex = 0);
    }

    nextImage(){

    }
    prevImage(){

    }


}

function listCards(){
    let cardgroup = document.createElement('div');
    cardgroup.classList.add('card-deck');
    cardgroup.id = 'cardgroup';
    document.getElementById('divMain').innerHTML = '';
    document.getElementById('divMain').appendChild(cardgroup);
    cardgroup.innerHTML = '';
    let div = document.getElementById("divLeft");
    div.innerHTML = '';
    function fillCardList(data){
        let list = document.createElement('ul');
        for(let i = 0; i < data.length; i++) {
            let element = document.createElement("li");
            element.innerHTML = `<a href="#" onclick="getCard(${data[i].id})">${data[i].norwegian} (${data[i].genus} ${data[i].latin})`;
            list.appendChild(element);
        }
        div.appendChild(list);
    }
    fetch('ajax.php?cards=true&listAllCards=true')
        .then(response => response.json())
        .then(data => fillCardList(data));
}

function getCard(id){

    card = new Cards(id);

    document.getElementById('cardgroup').appendChild(card.card);


}

function closeCard(id){
    cardToClose = document.getElementById(`card-${id}`);
    cardToClose.remove();
}

document.addEventListener('click', (e) =>{
    if(e.target.id.indexOf(`btnClose`) !== -1) {
        let id = e.target.id.split(/\s*\-\s*/g)[1];
        closeCard(id);
    }
    if(e.target.id.indexOf(`btnDeleteForveksling`) !== -1) {
        let id = e.target.id.split(/\s*\-\s*/g)[1];
        newSpeciesCard.delForveksling(id);
    }
    if(e.target.id.indexOf(`btnSave`) !== -1) {

        newSpeciesCard.saveCard();
    }
    if(e.target.id.indexOf(`submitImageFilename`) !== -1) {
        let imageDiv = document.getElementById('imagefiles');
        let imageFileName = document.getElementById('imageFile');
        newSpeciesCard.fileNames.push(imageFileName.value);
        imageDiv.innerHTML = imageDiv.innerHTML + imageFileName.value;
        imageFileName.value = '';
        e.preventDefault();
        return false;
    }
});