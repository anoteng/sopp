<?php
include('config.php');
include('classes.php');
header("Expires: 0");
header("Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0");
header("Pragma: no-cache");
header('Content-type: application/json; charset=UTF-8');

if(!isset($_SESSION['userid'])){
    die("You sneaky bastard!");
}
if(isset($_GET['cards'])){
    $cards = new cards();
    if(isset($_GET['listAllCards'])){
        if(isset($_GET['sortByNorwegian'])){
            echo json_encode($cards->listCardsByNorwegian());
        }else {
            echo json_encode($cards->listCards());
        }
    }
    if(isset($_GET['singlecard'])){
        echo $cards->getCard($_GET['singlecard']);
    }
    if(isset($_GET['imagelist'])){
        echo $cards->imageList($_GET['imagelist']);
    }
    if(isset($_GET['forvekslingsarter'])){
        echo $cards->forvekslingsarter($_GET['forvekslingsarter']);
    }
}
if(isset($_GET['newcard'])){
    $cards = new newCard();

    if(isset($_GET['spiselighet'])){
        echo json_encode($cards->getSpiselighet());
    }
    if(isset($_GET['sopptype'])){
        echo json_encode($cards->getSopptype());
    }
    if(isset($_GET['getall'])){
        echo json_encode($cards->getAll());
    }
    if(isset($_GET['genus'])){
        echo json_encode($cards->getGenus());
    }

    if(isset($_GET['saveCard'])){
//        echo json_encode($_POST);
        $post = json_decode(file_get_contents('php://input'), true);
        $id = $cards->saveNewCard($post);
        echo json_encode($id);
    }
    if(isset($_GET['debug'])){
        var_dump($_SESSION['post']);
    }
    if(isset($_GET['imageFileName'])){
        echo json_encode($cards->addImageFilename($_GET['id'], $_GET['imageFileName']));
    }

}

if(isset($_GET['quiz'])){
    $quiz = new quiz();

    if(isset($_GET['getCount'])){
        echo json_encode($quiz->getSpeciesCount());
    }

    if(isset($_GET['antall']) && isset($_GET['type']) && isset($_GET['kunnskap'])){
        echo json_encode($quiz->getQuizQuestions($_GET['antall'], $_GET['type'], $_GET['kunnskap']));
    }

    if(isset($_GET['alternatives'])){
        echo json_encode($quiz->getAlternatives($_GET['alternatives'], $_GET['not']));
    }

    if(isset($_GET['getBilder'])){
        echo json_encode($quiz->getImages($_GET['getBilder']));
    }
    if(isset($_GET['updateProgress'])){
        echo json_encode($quiz->updateProgress($_GET['updateProgress'], $_GET['value']));
    }
    if(isset($_GET['getStatus'])){
        echo json_encode($quiz->getStatus());
    }
}
if(isset($_GET['checkUserAccess'])){
    $uac = new userControl();
    echo json_encode($uac->checkUserAccess($_SESSION['userid']));
}
if(isset($_GET['getCurrentUserId'])){
    echo json_encode($_SESSION['userid']);
}

if(isset($_GET['image'])) {
    $images = new images();
    if (isset($_GET['getAllImages'])) echo json_encode($images->getAllImages());
    if (isset($_GET['getAllSpecies'])) echo json_encode($images->getAllSpecies());
    if (isset($_GET['getAllUsers'])) echo json_encode($images->getAllUsers());
    if (isset($_GET['getAllLicenses'])) echo json_encode($images->getAllLicenses());
    if (isset($_GET['rejectImage'])) echo json_encode($images->rejectImage($_GET['rejectImage']));
    if (isset($_GET['approveImage'])) echo json_encode($images->approveImage($_GET['approveImage']));
    if (isset($_GET['deleteImage'])) echo json_encode($images->deleteImage($_GET['deleteImage']));

}

if(isset($_GET['fileUpload'])){
    $images = new images();
    echo json_encode($images->newImageUpload($_FILES['newImage'], $_POST['species'], $_SESSION['userid'], $_POST['license']));
}