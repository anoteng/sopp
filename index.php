<?php

//Include Configuration File
include('config.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Sopp - pensumoversikt</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
<!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">-->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="js/login.js" async></script>

    <meta name="google-signin-client_id" content="620486776402-qg81g7o1vub6n35s3s6nk4911va2vpsm.apps.googleusercontent.com">
    <link rel="stylesheet" href="css/main.css">


</head>
<body>
<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href=".">Soppappen</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#" onclick="startQuiz()">Quiz</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#" onclick="visStatus()">Min læringsstatus</a>
                </li>
                <li class="nav-item active">
                    <a href="#" class="nav-link" onclick="listCards()">Vis alle artskort</a>
                </li>

                <li class="nav-item active" id="nyttArtskort" style="display: none">
                    <a href="#" class="nav-link" onclick="newSpecies()">Nytt artskort</a>
                </li>
                <li class="nav-item active" id="bilderBehandle" style="display: none">
                    <a href="#" class="nav-link" onclick="bilderOpplasting()">Behandle bilder</a>
                </li>
                <li class="nav-item">
                    <div class="g-signin2" data-onsuccess="onSignIn"></div>
                </li>

            </ul>
<!--            <form class="form-inline mt-2 mt-md-0">-->
<!--                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">-->
<!--                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>-->
<!--            </form>-->
        </div>
    </nav>
</header>
<main role="main" class="container">
    <div class="row" id="mainRow">
        <div class="col-2" id="divLeft">

        </div>
        <div class="col-10" id="divMain">
            <div class="card-deck" id="cardgroup">

            </div>

        </div>
    </div>

</main>
<!--<footer class="footer">-->
<!--    <div class="container">-->
<!--        <span class="text-muted">-->
<!--            copyright-info-->
<!--        </span>-->
<!--    </div>-->
<!--</footer>-->

<script>
    function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        let profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
        let id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '<?php echo $clientRedirectUri ?>');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            console.log('Signed in as: ' + xhr.responseText);
        };
        xhr.send('idtoken=' + id_token);
    }
</script>
<script src="js/cards.js"></script>
<script src="js/newCard.js"></script>
<script src="js/main.js"></script>
<script src="js/status.js"></script>
</body>
</html>