<?php
class sql{
    public $con;
    public $resultArray = [];

    function __construct(){
        $this->con = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
        if (!$this->con->set_charset("utf8")) {
            printf("Error loading character set utf8: %s\n", $this->con->connect_error);
        }
    }

    public function selectQuery($sql, $prepType = null, $prepVars = null){
        if(!$stmt = $this->con->prepare($sql)){
            die(var_dump($this->con->error_list));
        }

        if($prepVars){
            if(!$stmt->bind_param($prepType, $prepVars)){
                die($stmt->error);
            }
        }
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows == 0){
            return false;
        }

        while($row = $result->fetch_assoc()){
            $this->resultArray[] = $row;
        }

        return $this->resultArray;
    }

    public function selectQuery2($sql, $prepType, ...$prepVars){
        if(!$stmt = $this->con->prepare($sql)){
            die(var_dump($this->con->error_list));
        }
        if(!$stmt->bind_param($prepType, ...$prepVars)){
            die($stmt->error);
        }
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows == 0){
            return false;
        }

        while($row = $result->fetch_assoc()){
            $this->resultArray[] = $row;
        }

        return $this->resultArray;
    }

    public function insertQuery($sql, $prepType, ...$prepVars){
        $stmt = $this->con->prepare($sql);
        $stmt->bind_param($prepType, ...$prepVars) || die($stmt->error);
        $stmt->execute();
        return $stmt->insert_id;
    }
    public function updateQuery($sql, $prepType, ...$prepVars){
        $stmt = $this->con->prepare($sql);
        $stmt->bind_param($prepType, ...$prepVars) || die($stmt->error);
        $stmt->execute();
        //return $stmt->insert_id;
    }

}

class cards extends sql {
    public function listCards(){
        $sql = 'SELECT species.id, genus.latin AS genus, species.latin, species.norwegian FROM species ';
        $sql .= 'INNER JOIN genus ON species.genus=genus.id ORDER BY genus.latin, species.norwegian';
        return $this->selectQuery($sql);
    }
    public function listCardsByNorwegian(){
        $sql = 'SELECT species.id, genus.latin AS genus, species.latin, species.norwegian FROM species ';
        $sql .= 'INNER JOIN genus ON species.genus=genus.id ORDER BY species.norwegian';
        return $this->selectQuery($sql);
    }

    public function getCard($id){
        $sql = 'SELECT species.*, genus.latin AS genus, type.type AS typeDesc, edible.description AS edibleDesc ';
        $sql .= 'FROM species INNER JOIN genus ON species.genus=genus.id ';
        $sql .= 'INNER JOIN edible ON species.edible=edible.id ';
        $sql .= 'INNER JOIN type ON species.type=type.id ';
        $sql .= 'WHERE species.id = ?';

        $response = $this->selectQuery($sql, 'i', $id);

        return json_encode($response[0]);
    }

    public function imageList($id){
        $sql = "SELECT * FROM bilder WHERE species = ?";
        $response = $this->selectQuery($sql, 'i', $id);
        return json_encode($response);
    }

    public function forvekslingsarter($id){
        $sql = 'SELECT forvekslingsarter.*, species.norwegian FROM forvekslingsarter INNER JOIN species ON forvekslingsarter.species = species.id WHERE forvekslingsarter.id = ?';
        $response = $this->selectQuery($sql, 'i', $id);
        if($response) {
            $responsestring = '';
            foreach ($response as $row) {
                if ($responsestring != '') {
                    $responsestring .= ', ';
                }
                $responsestring .= "<a href='#' onclick='getCard({$row['species']})'>{$row['norwegian']}</a>";
            }
        }else{
            $responsestring = '';
        }
        return json_encode($responsestring);
    }
}

class newCard extends sql {
    public function save(){

    }

    public function getSpiselighet(){
        $sql = "SELECT * FROM edible";
        return $this->selectQuery($sql);
    }
    public function getSopptype(){
        $sql = "SELECT * FROM type";
        return $this->selectQuery($sql);
    }

    public function getAll(){
        $sql = "SELECT id, norwegian FROM species ORDER BY norwegian";
        return $this->selectQuery($sql);
    }

    public function getGenus(){
        $sql = "SELECT id, latin FROM genus ORDER BY latin";
        return $this->selectQuery($sql);
    }

    public function saveNewCard($arr){
        $sql = "INSERT INTO species (latin, norwegian, english, genus, hatt, type, stilk, sporepulver, voksested, merknad, edible, underside, forgiftningssymptomer) 
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        //$prepVarsString = "'{$arr['latin']}', '{$arr['norsk']}', '{$arr['english']}', '{$arr['genus']}', '{$arr['hatt']}', '{$arr['type']}', '{$arr['stilk']}', '{$arr['sporepulver']}', '{$arr['voksested']}', '{$arr['merknad']}', '{$arr['spiselighet']}', '{$arr['underside']}', '{$arr['giftsymptomer']}'";
        $prepTypeString = ('sssisissssiss');
        $id = $this->insertQuery($sql, $prepTypeString, $arr['latin'], $arr['norsk'], $arr['english'], $arr['genus'], $arr['hatt'], $arr['type'], $arr['stilk'], $arr['sporepulver'], $arr['voksested'], $arr['merknad'], $arr['spiselighet'], $arr['underside'], $arr['giftsymptomer']);
        //error_log($id);
        $forvekslingsarter = $arr['forvekslingsarter'];
        if(!empty($forvekslingsarter)){
            foreach($forvekslingsarter as $row){
                //error_log(print_r($row,true));
                $sql = "INSERT INTO forvekslingsarter (id, species) VALUES (?, ?)";
                $this->insertQuery($sql, 'ii', $id, $row['id']);
            }
        }
        return $id;
    }

    public function saveChangedCard($arr){

    }

    public function addImageFilename($id, $filename)
    {
        $sql = "INSERT INTO bilder (species, filename) VALUES (?, ?)";
        return $this->insertQuery($sql, 'is', $id, $filename);
    }
}

class quiz extends sql{

    public function getSpeciesCount(){
        $sql = "SELECT id FROM species";
        return count($this->selectQuery($sql));
    }
    public function getQuizQuestions($antall, $type, $kunnskap){
        if($kunnskap == 'true'){
            if($type == 'bilde'){
                $sql = "SELECT species.id, species.norwegian, species.latin, genus.latin as genus, edible.description as edibleDesc, species.edible FROM species
                        INNER JOIN edible on species.edible=edible.id
                        INNER JOIN genus on species.genus=genus.id
                        WHERE species.id not in (select species from quizProgress where id = ? and level > 4)
                        ORDER BY RAND()
                        LIMIT ?";
                return $this->selectQuery2($sql, 'ii', $_SESSION['userid'], $antall);
            }
        }else{
            if($type == 'bilde'){
                $sql = "SELECT species.id, species.norwegian, species.latin, genus.latin as genus, edible.description as edibleDesc, species.edible FROM species
                        INNER JOIN edible on species.edible=edible.id
                        INNER JOIN genus on species.genus=genus.id
                        ORDER BY RAND()
                        LIMIT ?";
                return $this->selectQuery($sql, 'i', $antall);
            }
        }
    }

    public function getAlternatives($antall, $not){
        $sql = "SELECT species.id as id, species.norwegian as norwegian, species.latin as latin, genus.latin as genus FROM species 
                INNER JOIN genus ON species.genus=genus.id 
                WHERE species.id <> ?
                ORDER BY RAND()
                LIMIT ?";
        return $this->selectQuery2($sql, 'ii', $not, $antall);
    }

    public function getImages($id){
        $sql = "SELECT id, filename FROM bilder WHERE species = ? ORDER BY RAND() LIMIT 1";
        return $this->selectQuery($sql, 'i', $id);

    }

    public function updateProgress($id, $value){
        $sql = "SELECT level FROM quizProgress WHERE id = ? AND species = ?";
        $result = $this->selectQuery2($sql, 'ii', $_SESSION['userid'], $id);
        if($result){
            $SQL = "UPDATE quizProgress SET level=level + ? WHERE id = ? AND species = ?";
            $this->insertQuery($SQL, 'iii', $value, $_SESSION['userid'], $id);
        }else{
            $SQL = "INSERT INTO quizProgress (id, species, level) VALUES (?, ?, ?)";
            $this->insertQuery($SQL, 'iii', $_SESSION['userid'], $id, $value);
        }
    }

    public function getStatus(){
        $sql = "SELECT species.norwegian, species.english, species.latin, genus.latin as genus, quizProgress.level FROM species ";
        $sql .= "INNER JOIN genus ON species.genus=genus.id ";
        $sql .= "INNER JOIN quizProgress ON species.id=quizProgress.species ";
        $sql .= "WHERE quizProgress.id = ?";
        return $this->selectQuery($sql, 'i', $_SESSION['userid']);

    }
}
class userControl extends sql{

    public function checkUserExists($userId){
        $sql = "SELECT id from users WHERE googleUserid = ?";
        $result = $this->selectQuery($sql, 's', $userId);
//        echo "googleuserid = $userId, result = $result";
//        echo var_dump($result);
        if($result[0]['id']){
            return $result[0]['id'];
        }else{
            return false;
        }
    }

    public function createUser($data){
        $userId = $data['sub'];
        $email = $data['email'];
        $name = $data['name'];
        $sql = "INSERT INTO users (googleUserid, email, name) VALUES (?, ?, ?)";
        echo $userId, $email, $name;
        return $this->insertQuery($sql, 'sss', $userId, $email, $name);
    }

    public function checkUserAccess($userid){
        $sql = "SELECT level FROM access WHERE id = ?";
        $result = $this->selectQuery($sql, 'i', $userid);
        //error_log(print_r($result[0], true));
        return $result[0]['level'];

    }


}
class images extends sql {
    public function getAllImages(){
        $sql = "SELECT * FROM bilder";
        $result = $this->selectQuery($sql);
        return $result;
    }
    public function getAllSpecies(){
        $sql = "SELECT species.id, species.norwegian, species.latin, genus.latin AS genus FROM species ";
        $sql .= "INNER JOIN genus ON species.genus = genus.id ";
        $sql .= "ORDER BY norwegian";
        $result = $this->selectQuery($sql);
        return $result;
    }

    public function getAllUsers(){
        $sql = "SELECT users.email, users.name, users.id FROM users ";
        $result = $this->selectQuery($sql);
        return $result;
    }

    public function getAllLicenses(){
        $sql = "SELECT * FROM license";
        $result = $this->selectQuery($sql);
        return $result;
    }

    public function newImageUpload($file, $species, $userid, $license){
        $uploaddir = '/var/www/html/images/';

        $uploadfile = $uploaddir . basename($file['name']);

        if (move_uploaded_file($file['tmp_name'], $uploadfile)){
        $sql = "INSERT INTO bilder (species, copyright, license, filename) VALUES (?, ?, ?, ?)";
        $result = $this->insertQuery($sql, 'iiis', $species, $userid, $license, basename($file['name']));
        error_log(print_r($file, true));
        return $result;
        }else{
                echo '<pre>';
                echo 'Here is some more debugging info:';
                print_r($file);

                print "</pre>";
            }
    }

    public function deleteImage($id){
        $sql = "DELETE FROM bilder WHERE id = ?";
        $this->updateQuery($sql, 'i', $id);
        return true;
    }
    public function approveImage($id){
        $sql = "UPDATE bilder SET approved = 1 WHERE id = ?";
        $this->updateQuery($sql, 'i', $id);
        return true;
    }
    public function rejectImage($id){
        $sql = "UPDATE bilder SET approved = 3 WHERE id = ?";
        $this->updateQuery($sql, 'i', $id);
        return true;
    }
}