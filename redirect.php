<?php

//index.php

//Include Configuration File
include('config.php');
include ('classes.php');
require_once 'vendor/autoload.php';

// Get $id_token via HTTPS POST.
$id_token = $_POST['idtoken'];

/** @noinspection PhpUndefinedVariableInspection */
$client = new Google_Client(['client_id' => $clientId]);  // Specify the CLIENT_ID of the app that accesses the backend
$payload = $client->verifyIdToken($id_token);
$users = new userControl();
if ($payload) {
    $_SESSION['googleUserid'] = $payload['sub'];
    $userid = $users->checkUserExists($_SESSION['googleUserid']);
    if($userid){
        //what to do if user allready exists...
        $_SESSION['userid'] = $userid;
        $_SESSION['accessLevel'] = $users->checkUserAccess($userid);
        echo $userid;
    }else{
        $_SESSION['userid'] = $users->createUser($payload);
        echo $_SESSION['userid'];
    }
} else {
    // Invalid ID token
}
