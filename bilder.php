Funksjonen er under utvikling
<div class="row hidden bildebehandling">
    <select id="artsliste">
        <option value="">-</option>
    </select>
    <div id="unapprovedSpecies"></div>
</div>
<div class="row hidden bildebehandling">
    <div class="card-group" id="bilderPerArt"></div>
</div>
<div id="uploadDiv" class="row hidden">
<div id="uploadCard" class="card">
    <img src="" class="card-img-top" id="uploadedImg">
    <div id="uploadCardbody" class="card-body">
    <input type="file" accept="image/*" name="newImage" data-max-file-size="3MB" />
    <div class="hidden" id="upladedImgProperties">
        <p id="copyright"></p>
        <p id="license"><select id="uploadImgLicense"></select></p>
        <button id="saveImage">Lagre bilde</button>

    </div>

    </div>
</div>
</div>